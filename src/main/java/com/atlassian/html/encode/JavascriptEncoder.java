package com.atlassian.html.encode;

import java.io.IOException;
import java.io.Writer;

import com.atlassian.annotations.PublicApi;

/**
 * Utility for encoding strings to be written into JavaScript.
 */
@PublicApi
public class JavascriptEncoder {
    /**
     * Encodes the given string as javascript
     * @param out output writer
     * @param str string to encode
     * @throws IOException from the writer
     */
    public static void escape(Writer out, String str) throws IOException {
        int len = str.length();
        for (int i = 0; i < len; ++i) {
            escape(out, str.charAt(i));
        }
    }

    /**
     * Encodes the given char array as javascript
     * @param out output writer
     * @param chars array to encode
     * @param off array offset
     * @param len number of chars to encode
     * @throws IOException from the writer
     */
    public static void escape(Writer out, char[] chars, int off, int len) throws IOException {
        for (int i = 0; i < len; ++i) {
            escape(out, chars[off + i]);
        }
    }

    private static void escape(Writer out, char c) throws IOException {
        // catch things that break rule 1.a above
        if (c == '"') { // doublequote
            out.write("\\u0022");
        } else if (c == '\'') { // single quote
            out.write("\\u0027");
        } else if (c == '\\') { // backslash
            out.write("\\u005C");
        } else if (c == 0x000A) { // various lineterminators
            out.write("\\n");
        } else if (c == 0x000D) {
            out.write("\\r");
        } else if (c == 0x2028) {
            out.write("\\u2028");
        } else if (c == 0x2029) {
            out.write("\\u2029");
        } else if (!Util.isPrintableAscii(c) || escapeAnyway(c)) {

            // be as nice as possible for non-ascii chars

            String hex = Integer.toHexString(c);

            out.write("\\u");

            int pad = 4;
            while (pad > hex.length()) {
                out.write('0');
                pad--;
            }
            out.write(hex);
        } else {
            out.write(c);
        }
    }

    /**
     * we need to escape '</script>' found inside javascript strings, but escape anything that looks
     * like html for good measure.
     */
    private static boolean escapeAnyway(int c) {
        // inside a javascript string,
        // '<': to prevent a html '</script>' tag from being parsed
        // '>': to prevent a html comment end tag ('-->') from being parsed
        return "<>".indexOf(c) != -1;
    }
}
