package com.atlassian.html.encode;

class Util {
    public static boolean isPrintableAscii(char c) {
        return (0x20 <= c) && (c <= 0x7e); // space through to ~
    }
}
